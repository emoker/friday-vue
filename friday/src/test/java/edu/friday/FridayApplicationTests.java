package edu.friday;

import edu.friday.repository.SysMenuRepository;
import edu.friday.repository.SysRoleRepository;
import edu.friday.repository.SysUserRepository;
import edu.friday.service.SysRoleService;
import edu.friday.service.SysUserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class FridayApplicationTests {

    @Autowired
    SysUserRepository sysUserRepository;

    @Autowired
    SysUserService sysUserService;

    @Autowired
    SysRoleRepository sysRoleRepository;

    @Autowired
    SysRoleService sysRoleService;

    @Autowired
    SysMenuRepository sysMenuRepository;

    @Test
    void contextLoads() {
//        SysUser s = new SysUser();
//        s.setStatus("3");
//        s.setUserName("test");
//        ExampleMatcher matcher = ExampleMatcher.matching()
//                .withIgnoreCase()
//                .withMatcher("userName", match -> match.contains());

//        Example<SysUser> example = Example.of(s);

//        System.out.println(sysUserDAO.findAll(example, PageRequest.of(0,100)).toList());
//        System.out.println(sysUserDAO.findById(120L).get());
//        Long[] user = new Long[10];
//        Long[] role = new Long[10];
//        Arrays.fill(user, 233L);
//        for (int i = 0; i < role.length; i++) {
//            role[i] = (long) i;
//        }
//        SysUserVO u = new SysUserVO(233L);
//        u.setRoleIds(role);
//        sysUserService.insertUserRole(u);
//        System.out.println(sysRoleDAO.countUserRoleByRoleId(2L));
//        sysRoleDAO.selectRoleIdsByUserId(2L).stream().forEach(System.out::println);
//        sysRoleService.deleteRoleByIds(101L);
        sysMenuRepository.selectMenuTreeAll().stream().forEach(System.out::println);
    }


}
