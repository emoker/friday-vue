package edu.friday.repository;

import edu.friday.model.SysDictType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * 字典数据表 数据层
 */
@Repository
public interface SysDictTypeRepository extends JpaRepository<SysDictType, Long> {

}
