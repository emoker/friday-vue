package edu.friday.repository;

import edu.friday.repository.custom.GenTableCustomRepository;
import edu.friday.model.GenTable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * 表 数据层
 */
@Repository
public interface GenTableRepository extends JpaRepository<GenTable, Long>, GenTableCustomRepository {

    @Modifying
    @Query(value = " delete from gen_table where table_id in (:tableIds) ", nativeQuery = true)
    void deleteByIds(@Param("tableIds") Long[] tableIds);

}
