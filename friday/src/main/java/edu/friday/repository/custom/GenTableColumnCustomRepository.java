package edu.friday.repository.custom;

import edu.friday.model.dto.GenTableColumnDTO;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 表 数据层
 */
@Repository
public interface GenTableColumnCustomRepository {

    List<GenTableColumnDTO> selectDbTableColumnsByName(String tableName);
}
