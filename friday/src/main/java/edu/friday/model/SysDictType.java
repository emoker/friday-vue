package edu.friday.model;

import edu.friday.common.base.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * 字典类型表 sys_dict_type
 */
@Entity
@Table(name = "sys_dict_type")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SysDictType extends BaseModel {
    private static final long serialVersionUID = 1L;

    /**
     * 字典主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long dictId;

    /**
     * 字典名称
     */
    private String dictName;

    /**
     * 字典类型
     */
    private String dictType;

    /**
     * 状态（0正常 1停用）
     */
    private String status;

}
