package edu.friday.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * 代码生成业务字段表 gen_table_column
 *
 * @author ruoyi
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GenTableColumnDTO {
    private static final long serialVersionUID = 1L;

    /**
     * 列名称
     */
    @Id
    private String columnName;

    /**
     * 列描述
     */
    private String columnComment;

    /**
     * 列类型
     */
    private String columnType;

    /**
     * 是否主键（1是）
     */
    private String isPk;

    /**
     * 是否自增（1是）
     */
    private String isIncrement;

    /**
     * 是否必填（1是）
     */
    private String isRequired;

    /**
     * 排序
     */
    private Integer sort;

}