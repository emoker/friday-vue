package edu.friday.controller;

import edu.friday.common.result.RestResult;
import edu.friday.model.SysDictType;
import edu.friday.service.SysDictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

/**
 * @description:
 **/
@RequestMapping("system/dict")
@RestController
public class DictController {

    @Autowired
    SysDictService sysDictService;

    @GetMapping("/data/dictType/{dictType}")
    public RestResult getDickByKey(@PathVariable  String dictType) throws IOException {
        return RestResult.success(sysDictService.selectDictDataByType(dictType));
    }

    @GetMapping("/type/optionselect")
    public RestResult optionselect()
    {
        List<SysDictType> dictTypes = sysDictService.selectDictTypeAll();
        return RestResult.success(dictTypes);
    }

}
