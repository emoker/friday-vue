package edu.friday.service;

import edu.friday.common.result.TableDataInfo;
import edu.friday.model.SysRole;
import edu.friday.model.vo.SysRoleVO;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

/**
 * 角色业务层
 */
public interface SysRoleService {
    TableDataInfo selectRoleList(SysRoleVO role, Pageable page);

    Set<String> selectRolePermissionByUserId(Long userId);

    List<SysRole> selectRoleAll();

    List<Long> selectRoleListByUserId(Long userId);

    SysRole selectRoleById(Long roleId);

    String checkRoleNameUnique(SysRoleVO role);

    String checkRoleKeyUnique(SysRoleVO role);

    void checkRoleAllowed(SysRoleVO role);

    int countUserRoleByRoleId(Long roleId);

    @Transactional
    int insertRole(SysRoleVO role);

    @Transactional
    int updateRole(SysRoleVO role);

    @Transactional
    int updateRoleStatus(SysRoleVO role);

    @Transactional
    int authDataScope(SysRoleVO role);

    @Transactional
    int deleteRoleById(Long roleId);

    @Transactional
    int deleteRoleByIds(Long[] roleIds);

//        /**
//     * 根据条件分页查询角色数据
//     *
//     * @param role 角色信息
//     * @return 角色数据集合信息
//     */
//    List<SysRole> selectRoleList(SysRole role);
//
//    /**
//     * 根据用户ID查询角色
//     *
//     * @param userId 用户ID
//     * @return 权限列表
//     */
//    public Set<String> selectRolePermissionByUserId(Long userId);
//
//    /**
//     * 查询所有角色
//     *
//     * @return 角色列表
//     */
//    public List<SysRole> selectRoleAll();
//
//    /**
//     * 根据用户ID获取角色选择框列表
//     *
//     * @param userId 用户ID
//     * @return 选中角色ID列表
//     */
//    public List<Integer> selectRoleListByUserId(Long userId);
//
//    /**
//     * 通过角色ID查询角色
//     *
//     * @param roleId 角色ID
//     * @return 角色对象信息
//     */
//    public SysRole selectRoleById(Long roleId);
//
//    /**
//     * 校验角色名称是否唯一
//     *
//     * @param role 角色信息
//     * @return 结果
//     */
//    public String checkRoleNameUnique(SysRole role);
//
//    /**
//     * 校验角色权限是否唯一
//     *
//     * @param role 角色信息
//     * @return 结果
//     */
//    public String checkRoleKeyUnique(SysRole role);
//
//    /**
//     * 校验角色是否允许操作
//     *
//     * @param role 角色信息
//     */
//    public void checkRoleAllowed(SysRole role);
//
//    /**
//     * 通过角色ID查询角色使用数量
//     *
//     * @param roleId 角色ID
//     * @return 结果
//     */
//    public int countUserRoleByRoleId(Long roleId);
//
//    /**
//     * 新增保存角色信息
//     *
//     * @param role 角色信息
//     * @return 结果
//     */
//    public int insertRole(SysRole role);
//
//    /**
//     * 修改保存角色信息
//     *
//     * @param role 角色信息
//     * @return 结果
//     */
//    public int updateRole(SysRole role);
//
//    /**
//     * 修改角色状态
//     *
//     * @param role 角色信息
//     * @return 结果
//     */
//    public int updateRoleStatus(SysRole role);
//
//    /**
//     * 修改数据权限信息
//     *
//     * @param role 角色信息
//     * @return 结果
//     */
//    public int authDataScope(SysRole role);
//
//    /**
//     * 通过角色ID删除角色
//     *
//     * @param roleId 角色ID
//     * @return 结果
//     */
//    public int deleteRoleById(Long roleId);
//
//    /**
//     * 批量删除角色信息
//     *
//     * @param roleIds 需要删除的角色ID
//     * @return 结果
//     */
//    public int deleteRoleByIds(Long[] roleIds);
}
