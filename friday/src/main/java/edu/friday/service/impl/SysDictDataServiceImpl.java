package edu.friday.service.impl;

import edu.friday.repository.SysDictDataRepository;
import edu.friday.repository.SysDictTypeRepository;
import edu.friday.model.SysDictData;
import edu.friday.model.SysDictType;
import edu.friday.service.SysDictService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 用户 业务层处理
 */
@Service
public class SysDictDataServiceImpl implements SysDictService {
    private static final Logger log = LoggerFactory.getLogger(SysDictDataServiceImpl.class);
    @Autowired
    SysDictDataRepository sysDictDataRepository;
    @Autowired
    SysDictTypeRepository sysDictTypeRepository;

    /**
     * 根据字典类型查询字典数据
     *
     * @param dictType 字典类型
     * @return 字典数据集合信息
     */
    @Override
    public List<SysDictData> selectDictDataByType(String dictType) {
        SysDictData sysDictData = new SysDictData();
        sysDictData.setDictType(dictType);
        Example<SysDictData> example = Example.of(sysDictData);
        return sysDictDataRepository.findAll(example);
    }

    @Override
    public List<SysDictType> selectDictTypeAll() {
        return sysDictTypeRepository.findAll();
    }
}
