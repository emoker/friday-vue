package edu.friday.service.impl;

import edu.friday.common.core.text.Convert;
import edu.friday.repository.GenTableColumnRepository;
import edu.friday.model.GenTableColumn;
import edu.friday.model.vo.GenTableColumnVO;
import edu.friday.service.GenTableColumnService;
import edu.friday.utils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 业务字段 服务层实现
 */
@Service
public class GenTableColumnServiceImpl implements GenTableColumnService {
    @Autowired
    private GenTableColumnRepository genTableColumnRepository;

    /**
     * 查询业务字段列表
     *
     * @param tableId 业务字段编号
     * @return 业务字段集合
     */
    @Override
    public List<GenTableColumnVO> selectGenTableColumnListByTableId(Long tableId) {
        //	    return genTableColumnMapper.selectGenTableColumnListByTableId(tableId);
        // TODO get columwn list by table id
        GenTableColumn genTableColumn = new GenTableColumn(tableId);
        Example<GenTableColumn> example = Example.of(genTableColumn);
        List<GenTableColumn> list = genTableColumnRepository.findAll(example);
        List<GenTableColumnVO> columns = BeanUtils.copyProperties(list, GenTableColumnVO.class);
        return columns;
    }

    /**
     * 新增业务字段
     *
     * @param genTableColumn 业务字段信息
     * @return 结果
     */
    @Override
    public int insertGenTableColumn(GenTableColumnVO genTableColumn) {
        GenTableColumn column = new GenTableColumn();
        BeanUtils.copyProperties(genTableColumn, column);
        genTableColumnRepository.save(column);
        return 1;

    }

    /**
     * 修改业务字段
     *
     * @param genTableColumn 业务字段信息
     * @return 结果
     */
    @Override
    public int updateGenTableColumn(GenTableColumnVO genTableColumn) {
        GenTableColumn column = genTableColumnRepository.getOne(genTableColumn.getColumnId());
        if (null == column) {
            return 0;
        }
        BeanUtils.copyPropertiesIgnoreEmpty(genTableColumn, column);
        genTableColumnRepository.save(column);
        return 1;
    }

    /**
     * 删除业务字段对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteGenTableColumnByIds(String ids) {
        Long[] idArr = Convert.toLongArray(ids);
        genTableColumnRepository.deleteByIds(idArr);
        return 0;
    }
}