package edu.friday.service;

import edu.friday.model.SysDictData;
import edu.friday.model.SysDictType;

import java.util.List;

/**
 * 字典 业务层
 */
public interface SysDictService {

    List<SysDictData> selectDictDataByType(String dictType);

    List<SysDictType> selectDictTypeAll();

}
