/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50643
Source Host           : 127.0.0.1:3306
Source Database       : friday

Target Server Type    : MYSQL
Target Server Version : 50643
File Encoding         : 65001

Date: 2020-04-09 07:42:44
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `gen_table`
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table` (
  `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) DEFAULT '' COMMENT '表描述',
  `class_name` varchar(100) DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) DEFAULT NULL COMMENT '生成功能作者',
  `options` varchar(1000) DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COMMENT='代码生成业务表';

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table` VALUES ('22', 'sys_config', '参数配置表', 'SysConfig', null, 'edu.friday.system', 'system', 'config', '参数配置', 'emok', null, 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table` VALUES ('23', 'sys_dict_data', '字典数据表', 'SysDictData', null, 'edu.friday.system', 'system', 'data', '字典数据', 'emok', null, 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table` VALUES ('24', 'sys_dict_type', '字典类型表', 'SysDictType', null, 'edu.friday.system', 'system', 'type', '字典类型', 'emok', null, 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table` VALUES ('25', 'sys_logininfor', '系统访问记录', 'SysLogininfor', null, 'edu.friday.system', 'system', 'logininfor', '系统访问记录', 'emok', null, 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table` VALUES ('26', 'sys_menu', '菜单权限表', 'SysMenu', null, 'edu.friday.system', 'system', 'menu', '菜单权限', 'emok', null, 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table` VALUES ('27', 'sys_oper_log', '操作日志记录', 'SysOperLog', null, 'edu.friday.system', 'system', 'log', '操作日志记录', 'emok', null, 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table` VALUES ('28', 'sys_role', '角色信息表', 'SysRole', null, 'edu.friday.system', 'system', 'role', '角色信息', 'emok', null, 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table` VALUES ('29', 'sys_role_menu', '角色和菜单关联表', 'SysRoleMenu', null, 'edu.friday.system', 'system', 'menu', '角色和菜单关联', 'emok', null, 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table` VALUES ('30', 'sys_user', '用户信息表', 'SysUser', 'crud', 'edu.friday.system', 'system', 'user', '用户信息', 'emok', '{}', 'admin', '2020-04-04 18:23:16', null, '2020-04-06 21:27:32', null);
INSERT INTO `gen_table` VALUES ('31', 'sys_user_role', '用户和角色关联表', 'SysUserRole', null, 'edu.friday.system', 'system', 'role', '用户和角色关联', 'emok', null, 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table` VALUES ('33', 'sys_test', '测试', 'SysTest', 'crud', 'edu.friday.system', 'system', 'test', '测试', 'emok', '{}', 'admin', '2020-04-06 23:57:38', null, '2020-04-07 00:00:01', null);

-- ----------------------------
-- Table structure for `gen_table_column`
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column` (
  `column_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` varchar(64) DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) DEFAULT '' COMMENT '字典类型',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`column_id`)
) ENGINE=InnoDB AUTO_INCREMENT=231 DEFAULT CHARSET=utf8mb4 COMMENT='代码生成业务表字段';

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column` VALUES ('115', '22', 'config_id', '参数主键', 'int(5)', 'Integer', 'configId', '1', '1', null, '1', null, null, null, null, 'input', null, '1', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('116', '22', 'config_name', '参数名称', 'varchar(100)', 'String', 'configName', '0', '0', null, '1', '1', '1', '1', 'LIKE', 'input', null, '2', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('117', '22', 'config_key', '参数键名', 'varchar(100)', 'String', 'configKey', '0', '0', null, '1', '1', '1', '1', null, 'input', null, '3', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('118', '22', 'config_value', '参数键值', 'varchar(500)', 'String', 'configValue', '0', '0', null, '1', '1', '1', '1', null, 'textarea', null, '4', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('119', '22', 'config_type', '系统内置（Y是 N否）', 'char(1)', 'String', 'configType', '0', '0', null, '1', '1', '1', '1', null, 'select', null, '5', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('120', '22', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', null, '1', null, null, null, null, 'input', null, '6', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('121', '22', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', null, '1', null, null, null, null, 'datetime', null, '7', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('122', '22', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', null, '1', '1', null, null, null, 'input', null, '8', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('123', '22', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', null, '1', '1', null, null, null, 'datetime', null, '9', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('124', '22', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', null, '1', '1', '1', null, null, 'textarea', null, '10', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('125', '23', 'dict_code', '字典编码', 'bigint(20)', 'Long', 'dictCode', '1', '1', null, '1', null, null, null, null, 'input', null, '1', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('126', '23', 'dict_sort', '字典排序', 'int(4)', 'Integer', 'dictSort', '0', '0', null, '1', '1', '1', '1', null, 'input', null, '2', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('127', '23', 'dict_label', '字典标签', 'varchar(100)', 'String', 'dictLabel', '0', '0', null, '1', '1', '1', '1', null, 'input', null, '3', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('128', '23', 'dict_value', '字典键值', 'varchar(100)', 'String', 'dictValue', '0', '0', null, '1', '1', '1', '1', null, 'input', null, '4', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('129', '23', 'dict_type', '字典类型', 'varchar(100)', 'String', 'dictType', '0', '0', null, '1', '1', '1', '1', null, 'select', null, '5', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('130', '23', 'css_class', '样式属性（其他样式扩展）', 'varchar(100)', 'String', 'cssClass', '0', '0', null, '1', '1', '1', '1', null, 'input', null, '6', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('131', '23', 'list_class', '表格回显样式', 'varchar(100)', 'String', 'listClass', '0', '0', null, '1', '1', '1', '1', null, 'input', null, '7', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('132', '23', 'is_default', '是否默认（Y是 N否）', 'char(1)', 'String', 'isDefault', '0', '0', null, '1', '1', '1', '1', null, 'input', null, '8', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('133', '23', 'status', '状态（0正常 1停用）', 'char(1)', 'String', 'status', '0', '0', null, '1', '1', '1', '1', null, 'radio', null, '9', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('134', '23', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', null, '1', null, null, null, null, 'input', null, '6', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('135', '23', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', null, '1', null, null, null, null, 'datetime', null, '7', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('136', '23', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', null, '1', '1', null, null, null, 'input', null, '8', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('137', '23', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', null, '1', '1', null, null, null, 'datetime', null, '9', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('138', '23', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', null, '1', '1', '1', null, null, 'textarea', null, '10', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('139', '24', 'dict_id', '字典主键', 'bigint(20)', 'Long', 'dictId', '1', '1', null, '1', null, null, null, null, 'input', null, '1', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('140', '24', 'dict_name', '字典名称', 'varchar(100)', 'String', 'dictName', '0', '0', null, '1', '1', '1', '1', 'LIKE', 'input', null, '2', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('141', '24', 'dict_type', '字典类型', 'varchar(100)', 'String', 'dictType', '0', '0', null, '1', '1', '1', '1', null, 'select', null, '5', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('142', '24', 'status', '状态（0正常 1停用）', 'char(1)', 'String', 'status', '0', '0', null, '1', '1', '1', '1', null, 'radio', null, '9', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('143', '24', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', null, '1', null, null, null, null, 'input', null, '6', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('144', '24', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', null, '1', null, null, null, null, 'datetime', null, '7', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('145', '24', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', null, '1', '1', null, null, null, 'input', null, '8', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('146', '24', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', null, '1', '1', null, null, null, 'datetime', null, '9', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('147', '24', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', null, '1', '1', '1', null, null, 'textarea', null, '10', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('148', '25', 'info_id', '访问ID', 'bigint(20)', 'Long', 'infoId', '1', '1', null, '1', null, null, null, null, 'input', null, '1', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('149', '25', 'user_name', '用户账号', 'varchar(50)', 'String', 'userName', '0', '0', null, '1', '1', '1', '1', 'LIKE', 'input', null, '2', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('150', '25', 'ipaddr', '登录IP地址', 'varchar(50)', 'String', 'ipaddr', '0', '0', null, '1', '1', '1', '1', null, 'input', null, '3', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('151', '25', 'login_location', '登录地点', 'varchar(255)', 'String', 'loginLocation', '0', '0', null, '1', '1', '1', '1', null, 'input', null, '4', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('152', '25', 'browser', '浏览器类型', 'varchar(50)', 'String', 'browser', '0', '0', null, '1', '1', '1', '1', null, 'input', null, '5', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('153', '25', 'os', '操作系统', 'varchar(50)', 'String', 'os', '0', '0', null, '1', '1', '1', '1', null, 'input', null, '6', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('154', '25', 'status', '状态（0正常 1停用）', 'char(1)', 'String', 'status', '0', '0', null, '1', '1', '1', '1', null, 'radio', null, '9', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('155', '25', 'msg', '提示消息', 'varchar(255)', 'String', 'msg', '0', '0', null, '1', '1', '1', '1', null, 'input', null, '8', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('156', '25', 'login_time', '访问时间', 'datetime', 'Date', 'loginTime', '0', '0', null, '1', '1', '1', '1', null, 'datetime', null, '9', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('157', '26', 'menu_id', '菜单ID', 'bigint(20)', 'Long', 'menuId', '1', '1', null, '1', null, null, null, null, 'input', null, '1', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('158', '26', 'menu_name', '菜单名称', 'varchar(50)', 'String', 'menuName', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', null, '2', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('159', '26', 'parent_id', '父菜单ID', 'bigint(20)', 'Long', 'parentId', '0', '0', null, '1', '1', '1', '1', null, 'input', null, '3', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('160', '26', 'order_num', '显示顺序', 'int(4)', 'Integer', 'orderNum', '0', '0', null, '1', '1', '1', '1', null, 'input', null, '4', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('161', '26', 'path', '路由地址', 'varchar(200)', 'String', 'path', '0', '0', null, '1', '1', '1', '1', null, 'input', null, '5', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('162', '26', 'component', '组件路径', 'varchar(255)', 'String', 'component', '0', '0', null, '1', '1', '1', '1', null, 'input', null, '6', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('163', '26', 'is_frame', '是否为外链（0是 1否）', 'int(1)', 'Integer', 'isFrame', '0', '0', null, '1', '1', '1', '1', null, 'input', null, '7', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('164', '26', 'menu_type', '菜单类型（M目录 C菜单 F按钮）', 'char(1)', 'String', 'menuType', '0', '0', null, '1', '1', '1', '1', null, 'select', null, '8', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('165', '26', 'visible', '菜单状态（0显示 1隐藏）', 'char(1)', 'String', 'visible', '0', '0', null, '1', '1', '1', '1', null, 'input', null, '9', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('166', '26', 'perms', '权限标识', 'varchar(100)', 'String', 'perms', '0', '0', null, '1', '1', '1', '1', null, 'input', null, '10', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('167', '26', 'icon', '菜单图标', 'varchar(100)', 'String', 'icon', '0', '0', null, '1', '1', '1', '1', null, 'input', null, '11', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('168', '26', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', null, '1', null, null, null, null, 'input', null, '6', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('169', '26', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', null, '1', null, null, null, null, 'datetime', null, '7', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('170', '26', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', null, '1', '1', null, null, null, 'input', null, '8', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('171', '26', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', null, '1', '1', null, null, null, 'datetime', null, '9', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('172', '26', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', null, '1', '1', '1', null, null, 'textarea', null, '10', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('173', '27', 'oper_id', '日志主键', 'bigint(20)', 'Long', 'operId', '1', '1', null, '1', null, null, null, null, 'input', null, '1', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('174', '27', 'title', '模块标题', 'varchar(50)', 'String', 'title', '0', '0', null, '1', '1', '1', '1', null, 'input', null, '2', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('175', '27', 'business_type', '业务类型（0其它 1新增 2修改 3删除）', 'int(2)', 'Integer', 'businessType', '0', '0', null, '1', '1', '1', '1', null, 'select', null, '3', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('176', '27', 'method', '方法名称', 'varchar(100)', 'String', 'method', '0', '0', null, '1', '1', '1', '1', null, 'input', null, '4', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('177', '27', 'request_method', '请求方式', 'varchar(10)', 'String', 'requestMethod', '0', '0', null, '1', '1', '1', '1', null, 'input', null, '5', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('178', '27', 'operator_type', '操作类别（0其它 1后台用户 2手机端用户）', 'int(1)', 'Integer', 'operatorType', '0', '0', null, '1', '1', '1', '1', null, 'select', null, '6', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('179', '27', 'oper_name', '操作人员', 'varchar(50)', 'String', 'operName', '0', '0', null, '1', '1', '1', '1', 'LIKE', 'input', null, '7', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('180', '27', 'dept_name', '部门名称', 'varchar(50)', 'String', 'deptName', '0', '0', null, '1', '1', '1', '1', 'LIKE', 'input', null, '8', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('181', '27', 'oper_url', '请求URL', 'varchar(255)', 'String', 'operUrl', '0', '0', null, '1', '1', '1', '1', null, 'input', null, '9', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('182', '27', 'oper_ip', '主机地址', 'varchar(50)', 'String', 'operIp', '0', '0', null, '1', '1', '1', '1', null, 'input', null, '10', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('183', '27', 'oper_location', '操作地点', 'varchar(255)', 'String', 'operLocation', '0', '0', null, '1', '1', '1', '1', null, 'input', null, '11', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('184', '27', 'oper_param', '请求参数', 'varchar(2000)', 'String', 'operParam', '0', '0', null, '1', '1', '1', '1', null, 'textarea', null, '12', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('185', '27', 'json_result', '返回参数', 'varchar(2000)', 'String', 'jsonResult', '0', '0', null, '1', '1', '1', '1', null, 'textarea', null, '13', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('186', '27', 'status', '状态（0正常 1停用）', 'char(1)', 'String', 'status', '0', '0', null, '1', '1', '1', '1', null, 'radio', null, '9', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('187', '27', 'error_msg', '错误消息', 'varchar(2000)', 'String', 'errorMsg', '0', '0', null, '1', '1', '1', '1', null, 'textarea', null, '15', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('188', '27', 'oper_time', '操作时间', 'datetime', 'Date', 'operTime', '0', '0', null, '1', '1', '1', '1', null, 'datetime', null, '16', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('189', '28', 'role_id', '角色ID', 'bigint(20)', 'Long', 'roleId', '1', '1', null, '1', null, null, null, null, 'input', null, '1', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('190', '28', 'role_name', '角色名称', 'varchar(30)', 'String', 'roleName', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', null, '2', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('191', '28', 'role_key', '角色权限字符串', 'varchar(100)', 'String', 'roleKey', '0', '0', '1', '1', '1', '1', '1', null, 'input', null, '3', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('192', '28', 'role_sort', '显示顺序', 'int(4)', 'Integer', 'roleSort', '0', '0', '1', '1', '1', '1', '1', null, 'input', null, '4', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('193', '28', 'data_scope', '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）', 'char(1)', 'String', 'dataScope', '0', '0', null, '1', '1', '1', '1', null, 'input', null, '5', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('194', '28', 'status', '状态（0正常 1停用）', 'char(1)', 'String', 'status', '0', '0', null, '1', '1', '1', '1', null, 'radio', null, '9', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('195', '28', 'del_flag', '删除标志（0代表存在 2代表删除）', 'char(1)', 'String', 'delFlag', '0', '0', null, '1', null, null, null, null, 'input', null, '7', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('196', '28', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', null, '1', null, null, null, null, 'input', null, '6', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('197', '28', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', null, '1', null, null, null, null, 'datetime', null, '7', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('198', '28', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', null, '1', '1', null, null, null, 'input', null, '8', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('199', '28', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', null, '1', '1', null, null, null, 'datetime', null, '9', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('200', '28', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', null, '1', '1', '1', null, null, 'textarea', null, '10', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('201', '29', 'role_id', '角色ID', 'bigint(20)', 'Long', 'roleId', '1', '1', null, '1', null, null, null, null, 'input', null, '1', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('202', '29', 'menu_id', '菜单ID', 'bigint(20)', 'Long', 'menuId', '1', '1', null, '1', null, null, null, null, 'input', null, '1', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('203', '30', 'user_id', '用户ID', 'bigint(20)', 'Long', 'userId', '1', '1', null, '1', null, null, null, null, 'input', null, '1', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('204', '30', 'user_name', '用户账号', 'varchar(50)', 'String', 'userName', '0', '0', null, '1', '1', '1', '1', 'LIKE', 'input', null, '2', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('205', '30', 'nick_name', '用户昵称', 'varchar(30)', 'String', 'nickName', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', null, '3', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('206', '30', 'user_type', '用户类型（00系统用户）', 'varchar(2)', 'String', 'userType', '0', '0', null, '1', '1', '1', '1', null, 'select', null, '4', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('207', '30', 'email', '用户邮箱', 'varchar(50)', 'String', 'email', '0', '0', null, '1', '1', '1', '1', null, 'input', null, '5', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('208', '30', 'phonenumber', '手机号码', 'varchar(11)', 'String', 'phonenumber', '0', '0', null, '1', '1', '1', '1', null, 'input', null, '6', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('209', '30', 'sex', '用户性别（0男 1女 2未知）', 'char(1)', 'String', 'sex', '0', '0', null, '1', '1', '1', '1', null, 'select', null, '7', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('210', '30', 'avatar', '头像地址', 'varchar(100)', 'String', 'avatar', '0', '0', null, '1', '1', '1', '1', null, 'input', null, '8', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('211', '30', 'password', '密码', 'varchar(100)', 'String', 'password', '0', '0', null, '1', '1', '1', '1', null, 'input', null, '9', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('212', '30', 'status', '状态（0正常 1停用）', 'char(1)', 'String', 'status', '0', '0', null, '1', '1', '1', '1', null, 'radio', null, '9', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('213', '30', 'del_flag', '删除标志（0代表存在 2代表删除）', 'char(1)', 'String', 'delFlag', '0', '0', null, '1', null, null, null, null, 'input', null, '7', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('214', '30', 'login_ip', '最后登陆IP', 'varchar(50)', 'String', 'loginIp', '0', '0', null, '1', '1', '1', '1', null, 'input', null, '12', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('215', '30', 'login_date', '最后登陆时间', 'datetime', 'Date', 'loginDate', '0', '0', null, '1', '1', '1', '1', null, 'datetime', null, '13', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('216', '30', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', null, '1', null, null, null, null, 'input', null, '6', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('217', '30', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', null, '1', null, null, null, null, 'datetime', null, '7', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('218', '30', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', null, '1', '1', null, null, null, 'input', null, '8', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('219', '30', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', null, '1', '1', null, null, null, 'datetime', null, '9', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('220', '30', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', null, '1', '1', '1', null, null, 'textarea', null, '10', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('221', '31', 'user_id', '用户ID', 'bigint(20)', 'Long', 'userId', '1', '1', null, '1', null, null, null, null, 'input', null, '1', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('222', '31', 'role_id', '角色ID', 'bigint(20)', 'Long', 'roleId', '1', '1', null, '1', null, null, null, null, 'input', null, '1', 'admin', '2020-04-04 18:23:16', null, null, null);
INSERT INTO `gen_table_column` VALUES ('224', '33', 'id', '', 'int(11)', 'Long', 'id', '1', '1', null, '1', null, null, null, null, 'input', null, '1', 'admin', '2020-04-06 23:57:38', null, null, null);
INSERT INTO `gen_table_column` VALUES ('225', '33', 'value', '', 'varchar(255)', 'String', 'value', '0', '0', null, '1', '1', '1', '1', null, 'input', null, '2', 'admin', '2020-04-06 23:57:38', null, null, null);
INSERT INTO `gen_table_column` VALUES ('226', '33', 'create_by', '', 'varchar(255)', 'String', 'createBy', '0', '0', null, '1', null, null, null, null, 'input', null, '3', 'admin', '2020-04-06 23:57:38', null, null, null);
INSERT INTO `gen_table_column` VALUES ('227', '33', 'create_time', '', 'datetime', 'Date', 'createTime', '0', '0', null, '1', null, null, null, null, 'datetime', null, '4', 'admin', '2020-04-06 23:57:38', null, null, null);
INSERT INTO `gen_table_column` VALUES ('228', '33', 'update_by', '', 'varchar(255)', 'String', 'updateBy', '0', '0', null, '1', '1', null, null, null, 'input', null, '5', 'admin', '2020-04-06 23:57:38', null, null, null);
INSERT INTO `gen_table_column` VALUES ('229', '33', 'update_time', '', 'datetime', 'Date', 'updateTime', '0', '0', null, '1', '1', null, null, null, 'datetime', null, '6', 'admin', '2020-04-06 23:57:38', null, null, null);
INSERT INTO `gen_table_column` VALUES ('230', '33', 'remark', '', 'varchar(255)', 'String', 'remark', '0', '0', null, '1', '1', '1', null, null, 'input', null, '7', 'admin', '2020-04-06 23:57:38', null, null, null);

-- ----------------------------
-- Table structure for `sys_config`
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config` (
  `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='参数配置表';

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES ('1', '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES ('2', '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '初始化密码 123456');
INSERT INTO `sys_config` VALUES ('3', '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '深色主题theme-dark，浅色主题theme-light');

-- ----------------------------
-- Table structure for `sys_dict_data`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data` (
  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) DEFAULT '0' COMMENT '字典排序',
  `dict_label` varchar(100) DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COMMENT='字典数据表';

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES ('1', '1', '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '性别男');
INSERT INTO `sys_dict_data` VALUES ('2', '2', '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '性别女');
INSERT INTO `sys_dict_data` VALUES ('3', '3', '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '性别未知');
INSERT INTO `sys_dict_data` VALUES ('4', '1', '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '显示菜单');
INSERT INTO `sys_dict_data` VALUES ('5', '2', '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES ('6', '1', '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '正常状态');
INSERT INTO `sys_dict_data` VALUES ('7', '2', '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '停用状态');
INSERT INTO `sys_dict_data` VALUES ('8', '1', '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '正常状态');
INSERT INTO `sys_dict_data` VALUES ('9', '2', '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '停用状态');
INSERT INTO `sys_dict_data` VALUES ('10', '1', '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '默认分组');
INSERT INTO `sys_dict_data` VALUES ('11', '2', '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统分组');
INSERT INTO `sys_dict_data` VALUES ('12', '1', '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统默认是');
INSERT INTO `sys_dict_data` VALUES ('13', '2', '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统默认否');
INSERT INTO `sys_dict_data` VALUES ('14', '1', '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '通知');
INSERT INTO `sys_dict_data` VALUES ('15', '2', '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '公告');
INSERT INTO `sys_dict_data` VALUES ('16', '1', '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '正常状态');
INSERT INTO `sys_dict_data` VALUES ('17', '2', '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '关闭状态');
INSERT INTO `sys_dict_data` VALUES ('18', '1', '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '新增操作');
INSERT INTO `sys_dict_data` VALUES ('19', '2', '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '修改操作');
INSERT INTO `sys_dict_data` VALUES ('20', '3', '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '删除操作');
INSERT INTO `sys_dict_data` VALUES ('21', '4', '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '授权操作');
INSERT INTO `sys_dict_data` VALUES ('22', '5', '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '导出操作');
INSERT INTO `sys_dict_data` VALUES ('23', '6', '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '导入操作');
INSERT INTO `sys_dict_data` VALUES ('24', '7', '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '强退操作');
INSERT INTO `sys_dict_data` VALUES ('25', '8', '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '生成操作');
INSERT INTO `sys_dict_data` VALUES ('26', '9', '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '清空操作');
INSERT INTO `sys_dict_data` VALUES ('27', '1', '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '正常状态');
INSERT INTO `sys_dict_data` VALUES ('28', '2', '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '停用状态');

-- ----------------------------
-- Table structure for `sys_dict_type`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type` (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) DEFAULT '' COMMENT '字典类型',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`),
  UNIQUE KEY `dict_type` (`dict_type`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COMMENT='字典类型表';

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES ('1', '用户性别', 'sys_user_sex', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '用户性别列表');
INSERT INTO `sys_dict_type` VALUES ('2', '菜单状态', 'sys_show_hide', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES ('3', '系统开关', 'sys_normal_disable', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统开关列表');
INSERT INTO `sys_dict_type` VALUES ('4', '任务状态', 'sys_job_status', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '任务状态列表');
INSERT INTO `sys_dict_type` VALUES ('5', '任务分组', 'sys_job_group', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '任务分组列表');
INSERT INTO `sys_dict_type` VALUES ('6', '系统是否', 'sys_yes_no', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统是否列表');
INSERT INTO `sys_dict_type` VALUES ('7', '通知类型', 'sys_notice_type', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '通知类型列表');
INSERT INTO `sys_dict_type` VALUES ('8', '通知状态', 'sys_notice_status', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '通知状态列表');
INSERT INTO `sys_dict_type` VALUES ('9', '操作类型', 'sys_oper_type', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '操作类型列表');
INSERT INTO `sys_dict_type` VALUES ('10', '系统状态', 'sys_common_status', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '登录状态列表');

-- ----------------------------
-- Table structure for `sys_logininfor`
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor` (
  `info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `user_name` varchar(50) DEFAULT '' COMMENT '用户账号',
  `ipaddr` varchar(50) DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) DEFAULT '' COMMENT '操作系统',
  `status` char(1) DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) DEFAULT '' COMMENT '提示消息',
  `login_time` datetime DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`)
) ENGINE=InnoDB AUTO_INCREMENT=166 DEFAULT CHARSET=utf8mb4 COMMENT='系统访问记录';

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------
INSERT INTO `sys_logininfor` VALUES ('100', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '验证码错误', '2020-03-11 22:35:34');
INSERT INTO `sys_logininfor` VALUES ('101', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-03-11 22:35:41');
INSERT INTO `sys_logininfor` VALUES ('102', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-03-11 22:37:52');
INSERT INTO `sys_logininfor` VALUES ('103', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-03-11 22:55:08');
INSERT INTO `sys_logininfor` VALUES ('104', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '验证码已失效', '2020-03-11 23:10:07');
INSERT INTO `sys_logininfor` VALUES ('105', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '验证码已失效', '2020-03-11 23:10:10');
INSERT INTO `sys_logininfor` VALUES ('106', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '验证码已失效', '2020-03-11 23:12:39');
INSERT INTO `sys_logininfor` VALUES ('107', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '验证码已失效', '2020-03-11 23:12:40');
INSERT INTO `sys_logininfor` VALUES ('108', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '验证码已失效', '2020-03-11 23:12:40');
INSERT INTO `sys_logininfor` VALUES ('109', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '验证码已失效', '2020-03-11 23:12:41');
INSERT INTO `sys_logininfor` VALUES ('110', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '验证码已失效', '2020-03-11 23:12:41');
INSERT INTO `sys_logininfor` VALUES ('111', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '验证码已失效', '2020-03-11 23:12:42');
INSERT INTO `sys_logininfor` VALUES ('112', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-03-11 23:14:47');
INSERT INTO `sys_logininfor` VALUES ('113', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-03-11 23:14:51');
INSERT INTO `sys_logininfor` VALUES ('114', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-03-11 23:14:54');
INSERT INTO `sys_logininfor` VALUES ('115', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-03-11 23:15:57');
INSERT INTO `sys_logininfor` VALUES ('116', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-03-11 23:16:33');
INSERT INTO `sys_logininfor` VALUES ('117', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-03-11 23:17:13');
INSERT INTO `sys_logininfor` VALUES ('118', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-03-11 23:24:06');
INSERT INTO `sys_logininfor` VALUES ('119', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-03-11 23:24:24');
INSERT INTO `sys_logininfor` VALUES ('120', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-03-11 23:32:22');
INSERT INTO `sys_logininfor` VALUES ('121', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-03-11 23:32:45');
INSERT INTO `sys_logininfor` VALUES ('122', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '验证码已失效', '2020-03-11 23:47:38');
INSERT INTO `sys_logininfor` VALUES ('123', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-03-11 23:48:35');
INSERT INTO `sys_logininfor` VALUES ('124', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-03-11 23:50:09');
INSERT INTO `sys_logininfor` VALUES ('125', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-03-11 23:50:11');
INSERT INTO `sys_logininfor` VALUES ('126', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-03-14 15:22:27');
INSERT INTO `sys_logininfor` VALUES ('127', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-03-14 15:23:50');
INSERT INTO `sys_logininfor` VALUES ('128', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-03-14 15:23:54');
INSERT INTO `sys_logininfor` VALUES ('129', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-03-14 15:25:30');
INSERT INTO `sys_logininfor` VALUES ('130', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-03-14 15:25:33');
INSERT INTO `sys_logininfor` VALUES ('131', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '验证码已失效', '2020-03-14 15:26:04');
INSERT INTO `sys_logininfor` VALUES ('132', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-03-14 15:26:06');
INSERT INTO `sys_logininfor` VALUES ('133', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-03-14 15:27:03');
INSERT INTO `sys_logininfor` VALUES ('134', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-03-14 15:27:05');
INSERT INTO `sys_logininfor` VALUES ('135', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-03-14 15:27:33');
INSERT INTO `sys_logininfor` VALUES ('136', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-03-14 15:27:35');
INSERT INTO `sys_logininfor` VALUES ('137', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-03-14 15:27:40');
INSERT INTO `sys_logininfor` VALUES ('138', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '验证码已失效', '2020-03-14 15:28:35');
INSERT INTO `sys_logininfor` VALUES ('139', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-03-14 15:28:36');
INSERT INTO `sys_logininfor` VALUES ('140', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-03-14 15:28:42');
INSERT INTO `sys_logininfor` VALUES ('141', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-03-14 15:28:46');
INSERT INTO `sys_logininfor` VALUES ('142', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-03-14 15:29:55');
INSERT INTO `sys_logininfor` VALUES ('143', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-03-14 15:29:57');
INSERT INTO `sys_logininfor` VALUES ('144', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-03-14 15:41:55');
INSERT INTO `sys_logininfor` VALUES ('145', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-03-14 15:42:43');
INSERT INTO `sys_logininfor` VALUES ('146', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '验证码已失效', '2020-03-15 17:23:11');
INSERT INTO `sys_logininfor` VALUES ('147', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-03-15 17:23:14');
INSERT INTO `sys_logininfor` VALUES ('148', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-03-15 17:25:14');
INSERT INTO `sys_logininfor` VALUES ('149', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-03-15 17:25:18');
INSERT INTO `sys_logininfor` VALUES ('150', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-03-15 17:25:24');
INSERT INTO `sys_logininfor` VALUES ('151', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-03-15 17:25:28');
INSERT INTO `sys_logininfor` VALUES ('152', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-03-15 17:26:17');
INSERT INTO `sys_logininfor` VALUES ('153', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-03-15 17:26:26');
INSERT INTO `sys_logininfor` VALUES ('154', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-03-15 17:27:19');
INSERT INTO `sys_logininfor` VALUES ('155', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-03-15 17:34:46');
INSERT INTO `sys_logininfor` VALUES ('156', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-03-15 17:35:13');
INSERT INTO `sys_logininfor` VALUES ('157', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-03-15 23:57:18');
INSERT INTO `sys_logininfor` VALUES ('158', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '验证码已失效', '2020-03-16 21:54:58');
INSERT INTO `sys_logininfor` VALUES ('159', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-03-16 21:55:00');
INSERT INTO `sys_logininfor` VALUES ('160', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-03-16 21:55:23');
INSERT INTO `sys_logininfor` VALUES ('161', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-03-16 21:55:25');
INSERT INTO `sys_logininfor` VALUES ('162', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-03-16 22:19:55');
INSERT INTO `sys_logininfor` VALUES ('163', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-03-16 22:19:58');
INSERT INTO `sys_logininfor` VALUES ('164', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2020-03-16 22:21:39');
INSERT INTO `sys_logininfor` VALUES ('165', 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2020-03-16 22:21:41');

-- ----------------------------
-- Table structure for `sys_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) DEFAULT '0' COMMENT '父菜单ID',
  `order_num` int(4) DEFAULT '0' COMMENT '显示顺序',
  `path` varchar(200) DEFAULT '' COMMENT '路由地址',
  `component` varchar(255) DEFAULT NULL COMMENT '组件路径',
  `is_frame` int(1) DEFAULT '1' COMMENT '是否为外链（0是 1否）',
  `menu_type` char(1) DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `perms` varchar(100) DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2002 DEFAULT CHARSET=utf8mb4 COMMENT='菜单权限表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '系统管理', '0', '1', 'system', null, '1', 'M', '0', '', 'system', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统管理目录');
INSERT INTO `sys_menu` VALUES ('2', '系统监控', '0', '2', 'monitor', null, '1', 'M', '1', '', 'monitor', 'admin', '2018-03-16 11:33:00', 'system', '2020-04-07 00:31:53', '系统监控目录');
INSERT INTO `sys_menu` VALUES ('3', '系统工具', '0', '3', 'tool', null, '1', 'M', '0', '', 'tool', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统工具目录');
INSERT INTO `sys_menu` VALUES ('4', '若依官网', '0', '4', 'http://ruoyi.vip', null, '0', 'M', '1', '', 'guide', 'admin', '2018-03-16 11:33:00', 'admin', '2020-03-11 23:49:00', '若依官网地址');
INSERT INTO `sys_menu` VALUES ('100', '用户管理', '1', '1', 'user', 'system/user/index', '1', 'C', '0', 'system:user:list', 'user', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '用户管理菜单');
INSERT INTO `sys_menu` VALUES ('101', '角色管理', '1', '2', 'role', 'system/role/index', '1', 'C', '0', 'system:role:list', 'peoples', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '角色管理菜单');
INSERT INTO `sys_menu` VALUES ('102', '菜单管理', '1', '3', 'menu', 'system/menu/index', '1', 'C', '0', 'system:menu:list', 'tree-table', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '菜单管理菜单');
INSERT INTO `sys_menu` VALUES ('103', '部门管理', '1', '4', 'dept', 'system/dept/index', '1', 'C', '1', 'system:dept:list', 'tree', 'admin', '2018-03-16 11:33:00', 'admin', '2020-03-11 23:50:03', '部门管理菜单');
INSERT INTO `sys_menu` VALUES ('105', '字典管理', '1', '6', 'dict', 'system/dict/index', '1', 'C', '1', 'system:dict:list', 'dict', 'admin', '2018-03-16 11:33:00', 'system', '2020-04-05 15:49:06', '字典管理菜单');
INSERT INTO `sys_menu` VALUES ('106', '参数设置', '1', '7', 'config', 'system/config/index', '1', 'C', '1', 'system:config:list', 'edit', 'admin', '2018-03-16 11:33:00', 'admin', '2020-03-16 22:07:19', '参数设置菜单');
INSERT INTO `sys_menu` VALUES ('107', '通知公告', '1', '8', 'notice', 'system/notice/index', '1', 'C', '1', 'system:notice:list', 'message', 'admin', '2018-03-16 11:33:00', 'admin', '2020-03-11 23:49:36', '通知公告菜单');
INSERT INTO `sys_menu` VALUES ('108', '日志管理', '1', '9', 'log', 'system/log/index', '1', 'M', '1', '', 'log', 'admin', '2018-03-16 11:33:00', 'system', '2020-04-05 15:49:12', '日志管理菜单');
INSERT INTO `sys_menu` VALUES ('109', '在线用户', '2', '1', 'online', 'monitor/online/index', '1', 'C', '0', 'monitor:online:list', 'online', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '在线用户菜单');
INSERT INTO `sys_menu` VALUES ('110', '定时任务', '2', '2', 'job', 'monitor/job/index', '1', 'C', '1', 'monitor:job:list', 'job', 'admin', '2018-03-16 11:33:00', 'admin', '2020-03-11 23:50:50', '定时任务菜单');
INSERT INTO `sys_menu` VALUES ('111', '数据监控', '3', '3', 'druid', 'tool/druid/index', '1', 'C', '0', 'monitor:druid:list', 'druid', 'admin', '2018-03-16 11:33:00', 'system', '2020-04-09 07:35:49', '数据监控菜单');
INSERT INTO `sys_menu` VALUES ('112', '服务监控', '2', '4', 'server', 'monitor/server/index', '1', 'C', '1', 'monitor:server:list', 'server', 'admin', '2018-03-16 11:33:00', 'system', '2020-04-07 00:30:38', '服务监控菜单');
INSERT INTO `sys_menu` VALUES ('113', '表单构建', '3', '1', 'build', 'tool/build/index', '1', 'C', '1', 'tool:build:list', 'build', 'admin', '2018-03-16 11:33:00', 'admin', '2020-03-11 23:49:22', '表单构建菜单');
INSERT INTO `sys_menu` VALUES ('114', '代码生成', '3', '2', 'gen', 'tool/gen/index', '1', 'C', '0', 'tool:gen:list', 'code', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '代码生成菜单');
INSERT INTO `sys_menu` VALUES ('115', '系统接口', '3', '3', 'swagger', 'tool/swagger/index', '1', 'C', '0', 'tool:swagger:list', 'swagger', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统接口菜单');
INSERT INTO `sys_menu` VALUES ('500', '操作日志', '108', '1', 'operlog', 'monitor/operlog/index', '1', 'C', '0', 'monitor:operlog:list', 'form', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '操作日志菜单');
INSERT INTO `sys_menu` VALUES ('501', '登录日志', '108', '2', 'logininfor', 'monitor/logininfor/index', '1', 'C', '0', 'monitor:logininfor:list', 'logininfor', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '登录日志菜单');
INSERT INTO `sys_menu` VALUES ('1001', '用户查询', '100', '1', '', '', '1', 'F', '0', 'system:user:query', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1002', '用户新增', '100', '2', '', '', '1', 'F', '0', 'system:user:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1003', '用户修改', '100', '3', '', '', '1', 'F', '0', 'system:user:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1004', '用户删除', '100', '4', '', '', '1', 'F', '0', 'system:user:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1007', '重置密码', '100', '7', '', '', '1', 'F', '0', 'system:user:resetPwd', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1008', '角色查询', '101', '1', '', '', '1', 'F', '0', 'system:role:query', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1009', '角色新增', '101', '2', '', '', '1', 'F', '0', 'system:role:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1010', '角色修改', '101', '3', '', '', '1', 'F', '0', 'system:role:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1011', '角色删除', '101', '4', '', '', '1', 'F', '0', 'system:role:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1013', '菜单查询', '102', '1', '', '', '1', 'F', '0', 'system:menu:query', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1014', '菜单新增', '102', '2', '', '', '1', 'F', '0', 'system:menu:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1015', '菜单修改', '102', '3', '', '', '1', 'F', '0', 'system:menu:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1016', '菜单删除', '102', '4', '', '', '1', 'F', '0', 'system:menu:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1017', '部门查询', '103', '1', '', '', '1', 'F', '0', 'system:dept:query', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1018', '部门新增', '103', '2', '', '', '1', 'F', '0', 'system:dept:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1019', '部门修改', '103', '3', '', '', '1', 'F', '0', 'system:dept:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1020', '部门删除', '103', '4', '', '', '1', 'F', '0', 'system:dept:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1026', '字典查询', '105', '1', '#', '', '1', 'F', '0', 'system:dict:query', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1027', '字典新增', '105', '2', '#', '', '1', 'F', '0', 'system:dict:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1028', '字典修改', '105', '3', '#', '', '1', 'F', '0', 'system:dict:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1029', '字典删除', '105', '4', '#', '', '1', 'F', '0', 'system:dict:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1030', '字典导出', '105', '5', '#', '', '1', 'F', '0', 'system:dict:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1031', '参数查询', '106', '1', '#', '', '1', 'F', '0', 'system:config:query', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1032', '参数新增', '106', '2', '#', '', '1', 'F', '0', 'system:config:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1033', '参数修改', '106', '3', '#', '', '1', 'F', '0', 'system:config:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1034', '参数删除', '106', '4', '#', '', '1', 'F', '0', 'system:config:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1035', '参数导出', '106', '5', '#', '', '1', 'F', '0', 'system:config:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1036', '公告查询', '107', '1', '#', '', '1', 'F', '0', 'system:notice:query', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1037', '公告新增', '107', '2', '#', '', '1', 'F', '0', 'system:notice:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1038', '公告修改', '107', '3', '#', '', '1', 'F', '0', 'system:notice:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1039', '公告删除', '107', '4', '#', '', '1', 'F', '0', 'system:notice:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1040', '操作查询', '500', '1', '#', '', '1', 'F', '0', 'monitor:operlog:query', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1041', '操作删除', '500', '2', '#', '', '1', 'F', '0', 'monitor:operlog:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1042', '日志导出', '500', '4', '#', '', '1', 'F', '0', 'monitor:operlog:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1043', '登录查询', '501', '1', '#', '', '1', 'F', '0', 'monitor:logininfor:query', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1044', '登录删除', '501', '2', '#', '', '1', 'F', '0', 'monitor:logininfor:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1045', '日志导出', '501', '3', '#', '', '1', 'F', '0', 'monitor:logininfor:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1046', '在线查询', '109', '1', '#', '', '1', 'F', '0', 'monitor:online:query', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1047', '批量强退', '109', '2', '#', '', '1', 'F', '0', 'monitor:online:batchLogout', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1048', '单条强退', '109', '3', '#', '', '1', 'F', '0', 'monitor:online:forceLogout', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1049', '任务查询', '110', '1', '#', '', '1', 'F', '0', 'monitor:job:query', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1050', '任务新增', '110', '2', '#', '', '1', 'F', '0', 'monitor:job:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1051', '任务修改', '110', '3', '#', '', '1', 'F', '0', 'monitor:job:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1052', '任务删除', '110', '4', '#', '', '1', 'F', '0', 'monitor:job:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1053', '状态修改', '110', '5', '#', '', '1', 'F', '0', 'monitor:job:changeStatus', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1054', '任务导出', '110', '7', '#', '', '1', 'F', '0', 'monitor:job:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1055', '生成查询', '114', '1', '#', '', '1', 'F', '0', 'tool:gen:query', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1056', '生成修改', '114', '2', '#', '', '1', 'F', '0', 'tool:gen:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1057', '生成删除', '114', '3', '#', '', '1', 'F', '0', 'tool:gen:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1058', '导入代码', '114', '2', '#', '', '1', 'F', '0', 'tool:gen:import', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1059', '预览代码', '114', '4', '#', '', '1', 'F', '0', 'tool:gen:preview', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1060', '生成代码', '114', '5', '#', '', '1', 'F', '0', 'tool:gen:code', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('2000', 'test', '0', '0', 'system', null, '0', 'M', '1', '', 'theme', 'admin', '2020-03-14 16:29:13', 'system', '2020-04-07 00:33:53', '');
INSERT INTO `sys_menu` VALUES ('2001', 'erter', '2000', '0', 'user', 'system/user/index', '1', 'C', '0', 'system:user:list', '404', 'admin', '2020-03-14 16:35:28', 'system', '2020-04-07 00:33:47', '');

-- ----------------------------
-- Table structure for `sys_oper_log`
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log` (
  `oper_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) DEFAULT '' COMMENT '模块标题',
  `business_type` int(2) DEFAULT '0' COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) DEFAULT '' COMMENT '请求方式',
  `operator_type` int(1) DEFAULT '0' COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(50) DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) DEFAULT '' COMMENT '返回参数',
  `status` int(1) DEFAULT '0' COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`oper_id`)
) ENGINE=InnoDB AUTO_INCREMENT=168 DEFAULT CHARSET=utf8mb4 COMMENT='操作日志记录';

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
INSERT INTO `sys_oper_log` VALUES ('100', '在线用户', '3', 'com.ruoyi.project.monitor.controller.SysUserOnlineController.forceLogout()', 'DELETE', '1', null, null, '/monitor/online/5b672f3d-0aa3-4343-955c-33f628e6eb06', '127.0.0.1', '内网IP', '{tokenId=5b672f3d-0aa3-4343-955c-33f628e6eb06}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-11 22:37:44');
INSERT INTO `sys_oper_log` VALUES ('101', '菜单管理', '2', 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"1\",\"icon\":\"guide\",\"orderNum\":\"4\",\"menuName\":\"若依官网\",\"params\":{},\"parentId\":0,\"path\":\"http://ruoyi.vip\",\"children\":[],\"createTime\":1521171180000,\"updateBy\":\"admin\",\"isFrame\":\"0\",\"menuId\":4,\"menuType\":\"M\",\"perms\":\"\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-11 23:49:00');
INSERT INTO `sys_oper_log` VALUES ('102', '菜单管理', '2', 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"1\",\"icon\":\"build\",\"orderNum\":\"1\",\"menuName\":\"表单构建\",\"params\":{},\"parentId\":3,\"path\":\"build\",\"component\":\"tool/build/index\",\"children\":[],\"createTime\":1521171180000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":113,\"menuType\":\"C\",\"perms\":\"tool:build:list\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-11 23:49:22');
INSERT INTO `sys_oper_log` VALUES ('103', '菜单管理', '2', 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"1\",\"icon\":\"message\",\"orderNum\":\"8\",\"menuName\":\"通知公告\",\"params\":{},\"parentId\":1,\"path\":\"notice\",\"component\":\"system/notice/index\",\"children\":[],\"createTime\":1521171180000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":107,\"menuType\":\"C\",\"perms\":\"system:notice:list\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-11 23:49:36');
INSERT INTO `sys_oper_log` VALUES ('104', '菜单管理', '2', 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"1\",\"icon\":\"edit\",\"orderNum\":\"7\",\"menuName\":\"参数设置\",\"params\":{},\"parentId\":1,\"path\":\"config\",\"component\":\"system/config/index\",\"children\":[],\"createTime\":1521171180000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":106,\"menuType\":\"C\",\"perms\":\"system:config:list\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-11 23:49:45');
INSERT INTO `sys_oper_log` VALUES ('105', '菜单管理', '2', 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"1\",\"icon\":\"post\",\"orderNum\":\"5\",\"menuName\":\"岗位管理\",\"params\":{},\"parentId\":1,\"path\":\"post\",\"component\":\"system/post/index\",\"children\":[],\"createTime\":1521171180000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":104,\"menuType\":\"C\",\"perms\":\"system:post:list\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-11 23:49:51');
INSERT INTO `sys_oper_log` VALUES ('106', '菜单管理', '2', 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"tree\",\"orderNum\":\"4\",\"menuName\":\"部门管理\",\"params\":{},\"parentId\":1,\"path\":\"dept\",\"component\":\"system/dept/index\",\"children\":[],\"createTime\":1521171180000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":103,\"menuType\":\"C\",\"perms\":\"system:dept:list\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-11 23:49:56');
INSERT INTO `sys_oper_log` VALUES ('107', '菜单管理', '2', 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"1\",\"icon\":\"tree\",\"orderNum\":\"4\",\"menuName\":\"部门管理\",\"params\":{},\"parentId\":1,\"path\":\"dept\",\"component\":\"system/dept/index\",\"children\":[],\"createTime\":1521171180000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":103,\"menuType\":\"C\",\"perms\":\"system:dept:list\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-11 23:50:03');
INSERT INTO `sys_oper_log` VALUES ('108', '菜单管理', '2', 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"1\",\"icon\":\"job\",\"orderNum\":\"2\",\"menuName\":\"定时任务\",\"params\":{},\"parentId\":2,\"path\":\"job\",\"component\":\"monitor/job/index\",\"children\":[],\"createTime\":1521171180000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":110,\"menuType\":\"C\",\"perms\":\"monitor:job:list\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-11 23:50:50');
INSERT INTO `sys_oper_log` VALUES ('109', '角色管理', '1', 'com.ruoyi.project.system.controller.SysRoleController.add()', 'POST', '1', 'admin', null, '/system/role', '127.0.0.1', '内网IP', '{\"flag\":false,\"roleId\":100,\"admin\":false,\"params\":{},\"roleSort\":\"0\",\"createBy\":\"admin\",\"roleKey\":\"test\",\"roleName\":\"测试\",\"deptIds\":[],\"menuIds\":[1,100,1001,1002,1003,1004,1005,1006,1007,101,1008,1009,1010,1011,1012,102,1013,1014,1015,1016,103,1017,1018,1019,1020,104,1021,1022,1023,1024,1025,105,1026,1027,1028,1029,1030,106,1031,1032,1033,1034,1035,107,1036,1037,1038,1039,108,500,1040,1041,1042,501,1043,1044,1045,2,109,1046,1047,1048,110,1049,1050,1051,1052,1053,1054,111,112,3,113,114,1055,1056,1058,1057,1059,1060,115,4],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-14 15:37:17');
INSERT INTO `sys_oper_log` VALUES ('110', '角色管理', '2', 'com.ruoyi.project.system.controller.SysRoleController.changeStatus()', 'PUT', '1', 'admin', null, '/system/role/changeStatus', '127.0.0.1', '内网IP', '{\"flag\":false,\"roleId\":100,\"admin\":false,\"params\":{},\"updateBy\":\"admin\",\"status\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-14 15:37:38');
INSERT INTO `sys_oper_log` VALUES ('111', '角色管理', '2', 'com.ruoyi.project.system.controller.SysRoleController.changeStatus()', 'PUT', '1', 'admin', null, '/system/role/changeStatus', '127.0.0.1', '内网IP', '{\"flag\":false,\"roleId\":100,\"admin\":false,\"params\":{},\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-14 15:37:41');
INSERT INTO `sys_oper_log` VALUES ('112', '角色管理', '2', 'com.ruoyi.project.system.controller.SysRoleController.changeStatus()', 'PUT', '1', 'admin', null, '/system/role/changeStatus', '127.0.0.1', '内网IP', '{\"flag\":false,\"roleId\":100,\"admin\":false,\"params\":{},\"updateBy\":\"admin\",\"status\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-14 15:37:46');
INSERT INTO `sys_oper_log` VALUES ('113', '用户管理', '2', 'com.ruoyi.project.system.controller.SysUserController.changeStatus()', 'PUT', '1', 'admin', null, '/system/user/changeStatus', '127.0.0.1', '内网IP', '{\"admin\":true,\"params\":{},\"userId\":1,\"status\":\"1\"}', 'null', '1', '不允许操作超级管理员用户', '2020-03-14 15:54:16');
INSERT INTO `sys_oper_log` VALUES ('114', '用户管理', '1', 'com.ruoyi.project.system.controller.SysUserController.add()', 'POST', '1', 'admin', null, '/system/user', '127.0.0.1', '内网IP', '{\"phonenumber\":\"13192294560\",\"admin\":false,\"password\":\"$2a$10$EOzCeI5Ah2Vuv8U4C84ZTO6U6GaY13yJsipweCIR1epAhqc4C.33W\",\"postIds\":[],\"email\":\"test@qq.com\",\"nickName\":\"test\",\"sex\":\"0\",\"deptId\":100,\"params\":{},\"userName\":\"test\",\"userId\":100,\"createBy\":\"admin\",\"roleIds\":[2],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-14 15:55:16');
INSERT INTO `sys_oper_log` VALUES ('115', '用户管理', '2', 'com.ruoyi.project.system.controller.SysUserController.edit()', 'PUT', '1', 'admin', null, '/system/user', '127.0.0.1', '内网IP', '{\"roles\":[{\"flag\":false,\"roleId\":2,\"admin\":false,\"dataScope\":\"2\",\"params\":{},\"roleSort\":\"2\",\"roleKey\":\"common\",\"roleName\":\"普通角色\",\"status\":\"0\"}],\"phonenumber\":\"13192294560\",\"admin\":false,\"delFlag\":\"0\",\"password\":\"\",\"updateBy\":\"admin\",\"postIds\":[],\"loginIp\":\"\",\"email\":\"test@qq.com\",\"nickName\":\"test\",\"sex\":\"0\",\"deptId\":100,\"avatar\":\"\",\"dept\":{\"deptName\":\"若依科技\",\"leader\":\"若依\",\"deptId\":100,\"orderNum\":\"0\",\"params\":{},\"parentId\":0,\"children\":[],\"status\":\"0\"},\"params\":{},\"userName\":\"test\",\"userId\":100,\"createBy\":\"admin\",\"roleIds\":[2],\"createTime\":1584172516000,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-14 15:57:05');
INSERT INTO `sys_oper_log` VALUES ('116', '用户管理', '2', 'com.ruoyi.project.system.controller.SysUserController.resetPwd()', 'PUT', '1', 'admin', null, '/system/user/resetPwd', '127.0.0.1', '内网IP', '{\"admin\":false,\"password\":\"$2a$10$fLcdPrdYZn9hNhTCFfMxBe0YhUYzDZgcvR89UGxDA.AnyzK1r6av.\",\"updateBy\":\"admin\",\"params\":{},\"userId\":100}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-14 15:58:17');
INSERT INTO `sys_oper_log` VALUES ('117', '用户管理', '3', 'com.ruoyi.project.system.controller.SysUserController.remove()', 'DELETE', '1', 'admin', null, '/system/user/100', '127.0.0.1', '内网IP', '{userIds=100}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-14 16:00:06');
INSERT INTO `sys_oper_log` VALUES ('118', '用户管理', '1', 'com.ruoyi.project.system.controller.SysUserController.add()', 'POST', '1', 'admin', null, '/system/user', '127.0.0.1', '内网IP', '{\"phonenumber\":\"13111111111\",\"admin\":false,\"password\":\"$2a$10$EWDLVRkUmBX2If/eCgwjJODuDjAPiX7ZDKEDfTgfwrUh68/h3L/6S\",\"postIds\":[],\"email\":\"11@qqw.bb\",\"nickName\":\"111\",\"deptId\":100,\"params\":{},\"userName\":\"11\",\"userId\":101,\"createBy\":\"admin\",\"roleIds\":[],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-14 16:01:08');
INSERT INTO `sys_oper_log` VALUES ('119', '用户管理', '1', 'com.ruoyi.project.system.controller.SysUserController.add()', 'POST', '1', 'admin', null, '/system/user', '127.0.0.1', '内网IP', '{\"phonenumber\":\"13122222222\",\"admin\":false,\"password\":\"$2a$10$bws6QiPyKuqDGjgfivN6B.37WcvpUmyaj4RnmH3vHduSp7.nJXN7i\",\"postIds\":[],\"email\":\"22@Qq.mm\",\"nickName\":\"222\",\"deptId\":100,\"params\":{},\"userName\":\"2222\",\"userId\":102,\"createBy\":\"admin\",\"roleIds\":[],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-14 16:01:50');
INSERT INTO `sys_oper_log` VALUES ('120', '用户管理', '3', 'com.ruoyi.project.system.controller.SysUserController.remove()', 'DELETE', '1', 'admin', null, '/system/user/102,101', '127.0.0.1', '内网IP', '{userIds=102,101}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-14 16:02:01');
INSERT INTO `sys_oper_log` VALUES ('121', '用户管理', '1', 'com.ruoyi.project.system.controller.SysUserController.add()', 'POST', '1', 'admin', null, '/system/user', '127.0.0.1', '内网IP', '{\"phonenumber\":\"13222332222\",\"admin\":false,\"password\":\"248264emoker\",\"postIds\":[],\"email\":\"22@qq.com\",\"nickName\":\"2\",\"deptId\":100,\"params\":{},\"userName\":\"test\",\"roleIds\":[],\"status\":\"0\"}', '{\"msg\":\"新增用户\'test\'失败，登录账号已存在\",\"code\":500}', '0', null, '2020-03-14 16:02:57');
INSERT INTO `sys_oper_log` VALUES ('122', '用户管理', '1', 'com.ruoyi.project.system.controller.SysUserController.add()', 'POST', '1', 'admin', null, '/system/user', '127.0.0.1', '内网IP', '{\"phonenumber\":\"13222332222\",\"admin\":false,\"password\":\"$2a$10$bCUZogMaUn7Gmr3A2OE5Ue/PI3lqqRHJbVnS1b8bPB1smIXyIEOU6\",\"postIds\":[],\"email\":\"22@qq.com\",\"nickName\":\"2\",\"deptId\":100,\"params\":{},\"userName\":\"test11\",\"userId\":103,\"createBy\":\"admin\",\"roleIds\":[],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-14 16:03:02');
INSERT INTO `sys_oper_log` VALUES ('123', '用户管理', '3', 'com.ruoyi.project.system.controller.SysUserController.remove()', 'DELETE', '1', 'admin', null, '/system/user/103', '127.0.0.1', '内网IP', '{userIds=103}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-14 16:03:06');
INSERT INTO `sys_oper_log` VALUES ('124', '角色管理', '1', 'com.ruoyi.project.system.controller.SysRoleController.add()', 'POST', '1', 'admin', null, '/system/role', '127.0.0.1', '内网IP', '{\"flag\":false,\"roleId\":101,\"admin\":false,\"params\":{},\"roleSort\":\"0\",\"createBy\":\"admin\",\"roleKey\":\"tert3\",\"roleName\":\"tert3\",\"deptIds\":[],\"menuIds\":[],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-14 16:22:14');
INSERT INTO `sys_oper_log` VALUES ('125', '角色管理', '2', 'com.ruoyi.project.system.controller.SysRoleController.changeStatus()', 'PUT', '1', 'admin', null, '/system/role/changeStatus', '127.0.0.1', '内网IP', '{\"flag\":false,\"roleId\":100,\"admin\":false,\"params\":{},\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-14 16:23:54');
INSERT INTO `sys_oper_log` VALUES ('126', '角色管理', '2', 'com.ruoyi.project.system.controller.SysRoleController.edit()', 'PUT', '1', 'admin', null, '/system/role', '127.0.0.1', '内网IP', '{\"flag\":false,\"roleId\":101,\"admin\":false,\"dataScope\":\"1\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"0\",\"createTime\":1584174134000,\"updateBy\":\"admin\",\"roleKey\":\"tert3\",\"roleName\":\"tert3\",\"menuIds\":[1,100,1001,1002,1003,1004,1005,1006,1007,101,1008,1009,1010,1011,1012,102,1013,1014,1015,1016,103,1017,1018,1019,1020,104,1021,1022,1023,1024,1025,105,1026,1027,1028,1029,1030,106,1031,1032,1033,1034,1035,107,1036,1037,1038,1039,108,500,1040,1041,1042,501,1043,1044,1045],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-14 16:24:50');
INSERT INTO `sys_oper_log` VALUES ('127', '角色管理', '2', 'com.ruoyi.project.system.controller.SysRoleController.dataScope()', 'PUT', '1', 'admin', null, '/system/role/dataScope', '127.0.0.1', '内网IP', '{\"flag\":false,\"roleId\":101,\"admin\":false,\"dataScope\":\"1\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"0\",\"createTime\":1584174134000,\"roleKey\":\"tert3\",\"roleName\":\"tert3\",\"deptIds\":[],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-14 16:25:31');
INSERT INTO `sys_oper_log` VALUES ('128', '角色管理', '5', 'com.ruoyi.project.system.controller.SysRoleController.export()', 'GET', '1', 'admin', null, '/system/role/export', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"d7161ebf-e7ce-47dc-8d2b-ea80d403be91_角色数据.xlsx\",\"code\":200}', '0', null, '2020-03-14 16:26:07');
INSERT INTO `sys_oper_log` VALUES ('129', '角色管理', '3', 'com.ruoyi.project.system.controller.SysRoleController.remove()', 'DELETE', '1', 'admin', null, '/system/role/101,100', '127.0.0.1', '内网IP', '{roleIds=101,100}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-14 16:26:26');
INSERT INTO `sys_oper_log` VALUES ('130', '菜单管理', '1', 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"theme\",\"orderNum\":\"0\",\"menuName\":\"test\",\"params\":{},\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"M\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-14 16:29:13');
INSERT INTO `sys_oper_log` VALUES ('131', '菜单管理', '2', 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"theme\",\"orderNum\":\"0\",\"menuName\":\"test\",\"params\":{},\"parentId\":0,\"path\":\"\",\"children\":[],\"createTime\":1584174553000,\"updateBy\":\"admin\",\"isFrame\":\"0\",\"menuId\":2000,\"menuType\":\"M\",\"perms\":\"\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-14 16:29:41');
INSERT INTO `sys_oper_log` VALUES ('132', '菜单管理', '2', 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"1\",\"icon\":\"theme\",\"orderNum\":\"0\",\"menuName\":\"test\",\"params\":{},\"parentId\":0,\"path\":\"#\",\"children\":[],\"createTime\":1584174553000,\"updateBy\":\"admin\",\"isFrame\":\"0\",\"menuId\":2000,\"menuType\":\"M\",\"perms\":\"\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-14 16:32:44');
INSERT INTO `sys_oper_log` VALUES ('133', '菜单管理', '1', 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"404\",\"orderNum\":\"0\",\"menuName\":\"erter\",\"params\":{},\"parentId\":2000,\"component\":\"/\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-14 16:35:28');
INSERT INTO `sys_oper_log` VALUES ('134', '菜单管理', '2', 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"404\",\"orderNum\":\"0\",\"menuName\":\"erter\",\"params\":{},\"parentId\":2000,\"path\":\"\",\"component\":\"system/user/index\",\"children\":[],\"createTime\":1584174928000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2001,\"menuType\":\"C\",\"perms\":\"\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-14 16:35:48');
INSERT INTO `sys_oper_log` VALUES ('135', '菜单管理', '2', 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"404\",\"orderNum\":\"0\",\"menuName\":\"erter\",\"params\":{},\"parentId\":2000,\"path\":\"\",\"component\":\"system/user/index\",\"children\":[],\"createTime\":1584174928000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2001,\"menuType\":\"C\",\"perms\":\"system:user:list\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-14 16:35:56');
INSERT INTO `sys_oper_log` VALUES ('136', '菜单管理', '2', 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"404\",\"orderNum\":\"0\",\"menuName\":\"erter\",\"params\":{},\"parentId\":2000,\"path\":\"\",\"component\":\"system/user/index\",\"children\":[],\"createTime\":1584174928000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2001,\"menuType\":\"C\",\"perms\":\"system:user:list\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-14 16:37:02');
INSERT INTO `sys_oper_log` VALUES ('137', '菜单管理', '2', 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"theme\",\"orderNum\":\"0\",\"menuName\":\"test\",\"params\":{},\"parentId\":0,\"path\":\"#\",\"children\":[],\"createTime\":1584174553000,\"updateBy\":\"admin\",\"isFrame\":\"0\",\"menuId\":2000,\"menuType\":\"M\",\"perms\":\"\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-14 16:37:48');
INSERT INTO `sys_oper_log` VALUES ('138', '菜单管理', '2', 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"404\",\"orderNum\":\"0\",\"menuName\":\"erter\",\"params\":{},\"parentId\":2000,\"path\":\"user\",\"component\":\"system/user/index\",\"children\":[],\"createTime\":1584174928000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2001,\"menuType\":\"C\",\"perms\":\"system:user:list\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-14 16:40:01');
INSERT INTO `sys_oper_log` VALUES ('139', '菜单管理', '2', 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"404\",\"orderNum\":\"0\",\"menuName\":\"erter\",\"params\":{},\"parentId\":2000,\"path\":\"user\",\"component\":\"system/user/index\",\"children\":[],\"createTime\":1584174928000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2001,\"menuType\":\"C\",\"perms\":\"system:user:list\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-14 16:41:20');
INSERT INTO `sys_oper_log` VALUES ('140', '菜单管理', '2', 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"theme\",\"orderNum\":\"0\",\"menuName\":\"test\",\"params\":{},\"parentId\":0,\"path\":\"system\",\"children\":[],\"createTime\":1584174553000,\"updateBy\":\"admin\",\"isFrame\":\"0\",\"menuId\":2000,\"menuType\":\"M\",\"perms\":\"\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-14 16:41:50');
INSERT INTO `sys_oper_log` VALUES ('141', '菜单管理', '1', 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"orderNum\":\"1\",\"menuName\":\"特特\",\"params\":{},\"parentId\":2000,\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"M\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-15 17:29:17');
INSERT INTO `sys_oper_log` VALUES ('142', '菜单管理', '3', 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', '1', 'admin', null, '/system/menu/2002', '127.0.0.1', '内网IP', '{menuId=2002}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-15 17:29:23');
INSERT INTO `sys_oper_log` VALUES ('143', '菜单管理', '1', 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"orderNum\":\"1\",\"menuName\":\"味儿\",\"params\":{},\"parentId\":2000,\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"M\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-15 17:29:41');
INSERT INTO `sys_oper_log` VALUES ('144', '菜单管理', '3', 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', '1', 'admin', null, '/system/menu/2003', '127.0.0.1', '内网IP', '{menuId=2003}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-15 17:30:51');
INSERT INTO `sys_oper_log` VALUES ('145', '个人信息', '2', 'com.ruoyi.project.system.controller.SysProfileController.updateProfile()', 'PUT', '1', 'admin', null, '/system/user/profile', '127.0.0.1', '内网IP', '{\"roles\":[{\"flag\":false,\"roleId\":1,\"admin\":true,\"dataScope\":\"1\",\"params\":{},\"roleSort\":\"1\",\"roleKey\":\"admin\",\"roleName\":\"管理员\",\"status\":\"0\"}],\"phonenumber\":\"15888888888\",\"admin\":true,\"loginDate\":1521171180000,\"remark\":\"管理员\",\"delFlag\":\"0\",\"password\":\"$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2\",\"loginIp\":\"127.0.0.1\",\"email\":\"ry@163.com\",\"nickName\":\"若依\",\"sex\":\"1\",\"deptId\":103,\"avatar\":\"\",\"dept\":{\"deptName\":\"研发部门\",\"leader\":\"若依\",\"deptId\":103,\"orderNum\":\"1\",\"params\":{},\"parentId\":101,\"children\":[],\"status\":\"0\"},\"params\":{},\"userName\":\"admin\",\"userId\":1,\"createBy\":\"admin\",\"createTime\":1521171180000,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-15 17:33:47');
INSERT INTO `sys_oper_log` VALUES ('146', '个人信息', '2', 'com.ruoyi.project.system.controller.SysProfileController.updatePwd()', 'PUT', '1', 'admin', null, '/system/user/profile/updatePwd', '127.0.0.1', '内网IP', 'admin 123456', '{\"msg\":\"修改密码失败，旧密码错误\",\"code\":500}', '0', null, '2020-03-15 17:34:19');
INSERT INTO `sys_oper_log` VALUES ('147', '个人信息', '2', 'com.ruoyi.project.system.controller.SysProfileController.updatePwd()', 'PUT', '1', 'admin', null, '/system/user/profile/updatePwd', '127.0.0.1', '内网IP', '123456 123456', '{\"msg\":\"修改密码失败，旧密码错误\",\"code\":500}', '0', null, '2020-03-15 17:34:31');
INSERT INTO `sys_oper_log` VALUES ('148', '个人信息', '2', 'com.ruoyi.project.system.controller.SysProfileController.updatePwd()', 'PUT', '1', 'admin', null, '/system/user/profile/updatePwd', '127.0.0.1', '内网IP', 'admin123 123456', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-15 17:35:23');
INSERT INTO `sys_oper_log` VALUES ('149', '个人信息', '2', 'com.ruoyi.project.system.controller.SysProfileController.updatePwd()', 'PUT', '1', 'admin', null, '/system/user/profile/updatePwd', '127.0.0.1', '内网IP', '123456 admin123', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-15 17:36:06');
INSERT INTO `sys_oper_log` VALUES ('150', '用户头像', '2', 'com.ruoyi.project.system.controller.SysProfileController.avatar()', 'POST', '1', 'admin', null, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/avatar/2020/03/15/e50424a92b16fd8faaffa3566c818e62.jpeg\",\"code\":200}', '0', null, '2020-03-15 17:36:46');
INSERT INTO `sys_oper_log` VALUES ('151', '菜单管理', '2', 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"edit\",\"orderNum\":\"7\",\"menuName\":\"参数设置\",\"params\":{},\"parentId\":1,\"path\":\"config\",\"component\":\"system/config/index\",\"children\":[],\"createTime\":1521171180000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":106,\"menuType\":\"C\",\"perms\":\"system:config:list\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-15 23:57:37');
INSERT INTO `sys_oper_log` VALUES ('152', '用户管理', '2', 'com.ruoyi.project.system.controller.SysUserController.resetPwd()', 'PUT', '1', 'admin', null, '/system/user/resetPwd', '127.0.0.1', '内网IP', '{\"admin\":true,\"params\":{},\"userId\":1}', 'null', '1', '不允许操作超级管理员用户', '2020-03-16 21:59:21');
INSERT INTO `sys_oper_log` VALUES ('153', '用户管理', '2', 'com.ruoyi.project.system.controller.SysUserController.resetPwd()', 'PUT', '1', 'admin', null, '/system/user/resetPwd', '127.0.0.1', '内网IP', '{\"admin\":false,\"params\":{},\"userId\":2}', 'null', '1', '', '2020-03-16 21:59:24');
INSERT INTO `sys_oper_log` VALUES ('154', '用户管理', '2', 'com.ruoyi.project.system.controller.SysUserController.resetPwd()', 'PUT', '1', 'admin', null, '/system/user/resetPwd', '127.0.0.1', '内网IP', '{\"admin\":false,\"password\":\"$2a$10$LP25Pp2GITfV3QX6v.MmT.jV7vi.1rnQQJeeab3/AIyJn2.kPFiOq\",\"updateBy\":\"admin\",\"params\":{},\"userId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-16 21:59:37');
INSERT INTO `sys_oper_log` VALUES ('155', '用户管理', '2', 'com.ruoyi.project.system.controller.SysUserController.edit()', 'PUT', '1', 'admin', null, '/system/user', '127.0.0.1', '内网IP', '{\"roles\":[{\"flag\":false,\"roleId\":2,\"admin\":false,\"dataScope\":\"2\",\"params\":{},\"roleSort\":\"2\",\"roleKey\":\"common\",\"roleName\":\"普通角色\",\"status\":\"0\"}],\"phonenumber\":\"15666666666\",\"admin\":false,\"loginDate\":1521171180000,\"remark\":\"测试员\",\"delFlag\":\"0\",\"password\":\"\",\"updateBy\":\"admin\",\"loginIp\":\"127.0.0.1\",\"email\":\"ry@qq.com\",\"nickName\":\"若依\",\"sex\":\"1\",\"deptId\":105,\"avatar\":\"\",\"dept\":{\"deptName\":\"测试部门\",\"leader\":\"若依\",\"deptId\":105,\"orderNum\":\"3\",\"params\":{},\"parentId\":101,\"children\":[],\"status\":\"0\"},\"params\":{},\"userName\":\"ry\",\"userId\":2,\"createBy\":\"admin\",\"roleIds\":[2],\"createTime\":1521171180000,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-16 22:03:17');
INSERT INTO `sys_oper_log` VALUES ('156', '角色管理', '2', 'com.ruoyi.project.system.controller.SysRoleController.edit()', 'PUT', '1', 'admin', null, '/system/role', '127.0.0.1', '内网IP', '{\"flag\":false,\"roleId\":1,\"admin\":true,\"remark\":\"管理员\",\"dataScope\":\"1\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"1\",\"createTime\":1521171180000,\"roleKey\":\"admin\",\"roleName\":\"管理员\",\"menuIds\":[2000,2001,1,100,1001,1002,1003,1004,1005,1006,1007,101,1008,1009,1010,1011,1012,102,1013,1014,1015,1016,103,1017,1018,1019,1020,104,1021,1022,1023,1024,1025,105,1026,1027,1028,1029,1030,106,1031,1032,1033,1034,1035,107,1036,1037,1038,1039,108,500,1040,1041,1042,501,1043,1044,1045,2,109,1046,1047,1048,110,1049,1050,1051,1052,1053,1054,111,112,3,113,114,1055,1056,1058,1057,1059,1060,115,4],\"status\":\"0\"}', 'null', '1', '不允许操作超级管理员角色', '2020-03-16 22:06:09');
INSERT INTO `sys_oper_log` VALUES ('157', '菜单管理', '2', 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"1\",\"icon\":\"edit\",\"orderNum\":\"7\",\"menuName\":\"参数设置\",\"params\":{},\"parentId\":1,\"path\":\"config\",\"component\":\"system/config/index\",\"children\":[],\"createTime\":1521171180000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":106,\"menuType\":\"C\",\"perms\":\"system:config:list\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-16 22:07:19');
INSERT INTO `sys_oper_log` VALUES ('158', '角色管理', '2', 'com.ruoyi.project.system.controller.SysRoleController.edit()', 'PUT', '1', 'admin', null, '/system/role', '127.0.0.1', '内网IP', '{\"flag\":false,\"roleId\":2,\"admin\":false,\"remark\":\"普通角色\",\"dataScope\":\"2\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"2\",\"createTime\":1521171180000,\"updateBy\":\"admin\",\"roleKey\":\"common\",\"roleName\":\"普通角色\",\"menuIds\":[100,1001,1002,1003,1004,1005,1006,1007,101,1008,1009,1010,1011,1012,102,1013,1014,1015,1016,103,1017,1018,1019,1020,104,1021,1022,1023,1024,1025,105,1026,1027,1028,1029,1030,106,1031,1032,1033,1034,1035,107,1036,1037,1038,1039,1040,1042,1043,1045,2,109,1046,1047,1048,110,1049,1050,1051,1052,1053,1054,111,112,3,113,114,1055,1056,1058,1057,1059,1060,115,4,1,108,500,501],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-16 22:12:51');
INSERT INTO `sys_oper_log` VALUES ('159', '角色管理', '2', 'com.ruoyi.project.system.controller.SysRoleController.edit()', 'PUT', '1', 'admin', null, '/system/role', '127.0.0.1', '内网IP', '{\"flag\":false,\"roleId\":2,\"admin\":false,\"remark\":\"普通角色\",\"dataScope\":\"2\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"2\",\"createTime\":1521171180000,\"updateBy\":\"admin\",\"roleKey\":\"common\",\"roleName\":\"普通角色\",\"menuIds\":[100,1001,1002,1003,1004,1005,1006,1007,101,1008,1009,1010,1011,1012,1013,1014,103,1017,1018,1019,1020,104,1021,1022,1023,1024,1025,105,1026,1027,1028,1029,1030,106,1031,1032,1033,1034,1035,107,1036,1037,1038,1039,1040,1042,1043,1045,2,109,1046,1047,1048,110,1049,1050,1051,1052,1053,1054,111,112,3,113,114,1055,1056,1058,1057,1059,1060,115,4,1,102,108,500,501],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-16 22:13:18');
INSERT INTO `sys_oper_log` VALUES ('160', '用户管理', '1', 'com.ruoyi.project.system.controller.SysUserController.add()', 'POST', '1', 'admin', null, '/system/user', '127.0.0.1', '内网IP', '{\"phonenumber\":\"13315554215\",\"admin\":false,\"password\":\"123456\",\"email\":\"test@qq.com\",\"nickName\":\"test\",\"sex\":\"0\",\"params\":{},\"userName\":\"test\",\"roleIds\":[2],\"status\":\"0\"}', '{\"msg\":\"新增用户\'test\'失败，登录账号已存在\",\"code\":500}', '0', null, '2020-03-16 22:16:02');
INSERT INTO `sys_oper_log` VALUES ('161', '用户管理', '1', 'com.ruoyi.project.system.controller.SysUserController.add()', 'POST', '1', 'admin', null, '/system/user', '127.0.0.1', '内网IP', '{\"phonenumber\":\"13315554215\",\"admin\":false,\"password\":\"123456\",\"email\":\"test@qq.com\",\"nickName\":\"test1\",\"sex\":\"0\",\"params\":{},\"userName\":\"test\",\"roleIds\":[2],\"status\":\"0\"}', '{\"msg\":\"新增用户\'test\'失败，登录账号已存在\",\"code\":500}', '0', null, '2020-03-16 22:16:13');
INSERT INTO `sys_oper_log` VALUES ('162', '用户管理', '1', 'com.ruoyi.project.system.controller.SysUserController.add()', 'POST', '1', 'admin', null, '/system/user', '127.0.0.1', '内网IP', '{\"phonenumber\":\"13315554215\",\"admin\":false,\"password\":\"123456\",\"email\":\"test@qq.com\",\"nickName\":\"test1\",\"sex\":\"0\",\"params\":{},\"userName\":\"test1\",\"roleIds\":[2],\"status\":\"0\"}', '{\"msg\":\"新增用户\'test1\'失败，邮箱账号已存在\",\"code\":500}', '0', null, '2020-03-16 22:16:18');
INSERT INTO `sys_oper_log` VALUES ('163', '用户管理', '1', 'com.ruoyi.project.system.controller.SysUserController.add()', 'POST', '1', 'admin', null, '/system/user', '127.0.0.1', '内网IP', '{\"phonenumber\":\"13315554215\",\"admin\":false,\"password\":\"123456\",\"email\":\"test@qq.com\",\"nickName\":\"test11\",\"sex\":\"0\",\"params\":{},\"userName\":\"test11\",\"roleIds\":[2],\"status\":\"0\"}', '{\"msg\":\"新增用户\'test11\'失败，登录账号已存在\",\"code\":500}', '0', null, '2020-03-16 22:16:21');
INSERT INTO `sys_oper_log` VALUES ('164', '用户管理', '1', 'com.ruoyi.project.system.controller.SysUserController.add()', 'POST', '1', 'admin', null, '/system/user', '127.0.0.1', '内网IP', '{\"phonenumber\":\"13315554215\",\"admin\":false,\"password\":\"123456\",\"email\":\"test@qq.com\",\"nickName\":\"test111\",\"sex\":\"0\",\"params\":{},\"userName\":\"test111\",\"roleIds\":[2],\"status\":\"0\"}', '{\"msg\":\"新增用户\'test111\'失败，邮箱账号已存在\",\"code\":500}', '0', null, '2020-03-16 22:16:24');
INSERT INTO `sys_oper_log` VALUES ('165', '用户管理', '1', 'com.ruoyi.project.system.controller.SysUserController.add()', 'POST', '1', 'admin', null, '/system/user', '127.0.0.1', '内网IP', '{\"phonenumber\":\"13315554215\",\"admin\":false,\"password\":\"123456\",\"email\":\"test1@qq.com\",\"nickName\":\"test\",\"sex\":\"0\",\"params\":{},\"userName\":\"test\",\"roleIds\":[2],\"status\":\"0\"}', '{\"msg\":\"新增用户\'test\'失败，登录账号已存在\",\"code\":500}', '0', null, '2020-03-16 22:16:32');
INSERT INTO `sys_oper_log` VALUES ('166', '用户管理', '1', 'com.ruoyi.project.system.controller.SysUserController.add()', 'POST', '1', 'admin', null, '/system/user', '127.0.0.1', '内网IP', '{\"phonenumber\":\"13315554215\",\"admin\":false,\"password\":\"$2a$10$XbewXY9YtEf65z6bBz5Od.HoPPsfgFo8qsczSFf4AW1EMZb8jdtRy\",\"email\":\"test1@qq.com\",\"nickName\":\"test\",\"sex\":\"0\",\"params\":{},\"userName\":\"test1\",\"userId\":104,\"createBy\":\"admin\",\"roleIds\":[2],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-16 22:16:36');
INSERT INTO `sys_oper_log` VALUES ('167', '用户管理', '2', 'com.ruoyi.controller.system.SysUserController.edit()', 'PUT', '1', 'admin', null, '/system/user', '127.0.0.1', '内网IP', '{\"roles\":[{\"flag\":false,\"roleId\":2,\"admin\":false,\"dataScope\":\"2\",\"params\":{},\"roleSort\":\"2\",\"roleKey\":\"common\",\"roleName\":\"普通角色\",\"status\":\"0\"}],\"phonenumber\":\"13315554215\",\"admin\":false,\"delFlag\":\"0\",\"password\":\"\",\"updateBy\":\"admin\",\"loginIp\":\"\",\"email\":\"test1@qq.com\",\"nickName\":\"test\",\"sex\":\"0\",\"avatar\":\"\",\"params\":{},\"userName\":\"test1\",\"userId\":104,\"createBy\":\"admin\",\"roleIds\":[2],\"createTime\":1584368196000,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2020-03-16 22:22:44');

-- ----------------------------
-- Table structure for `sys_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `status` char(1) NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=utf8mb4 COMMENT='角色信息表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '管理员', 'admin', '1', '1', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '管理员');
INSERT INTO `sys_role` VALUES ('2', '普通角色', 'common', '2', '2', '0', '0', 'admin', '2018-03-16 11:33:00', 'system', '2020-04-02 07:58:06', '普通角色');
INSERT INTO `sys_role` VALUES ('100', '测试', 'test', '0', '1', '0', '0', 'admin', '2020-03-14 15:37:17', 'system', '2020-04-02 07:58:00', null);
INSERT INTO `sys_role` VALUES ('101', 'tstere', 'dfdf', '3', '1', '0', '2', '', null, 'system', '2020-04-07 00:44:49', null);
INSERT INTO `sys_role` VALUES ('102', 'sdfdf', 'sdf', '0', null, '0', '2', 'system', '2020-04-02 07:46:34', null, null, null);
INSERT INTO `sys_role` VALUES ('103', '水电费', '说的话说的话 ', '0', null, '0', '2', 'system', '2020-04-03 07:51:06', 'system', '2020-04-03 07:54:39', null);
INSERT INTO `sys_role` VALUES ('107', 'asdasd', 'asdasd', '0', null, '0', '2', 'system', '2020-04-03 07:58:37', null, null, null);
INSERT INTO `sys_role` VALUES ('108', 'asdasd', 'asdasd', '0', null, '0', '2', 'system', '2020-04-03 07:58:37', null, null, null);
INSERT INTO `sys_role` VALUES ('109', '奥迪', '奥迪', '0', null, '0', '2', 'system', '2020-04-03 07:59:05', null, null, null);

-- ----------------------------
-- Table structure for `sys_role_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='角色和菜单关联表';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_user`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `user_name` varchar(30) NOT NULL COMMENT '用户账号',
  `nick_name` varchar(30) NOT NULL COMMENT '用户昵称',
  `user_type` varchar(2) DEFAULT '00' COMMENT '用户类型（00系统用户）',
  `email` varchar(50) DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) DEFAULT '' COMMENT '手机号码',
  `sex` char(1) DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(500) DEFAULT '' COMMENT '头像地址',
  `password` varchar(100) DEFAULT '' COMMENT '密码',
  `status` char(1) DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(50) DEFAULT '' COMMENT '最后登陆IP',
  `login_date` datetime DEFAULT NULL COMMENT '最后登陆时间',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=utf8mb4 COMMENT='用户信息表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', '若依', '00', 'ry@163.com', '15888888888', '1', 'https://wx.qlogo.cn/mmopen/vi_32/1yzTKJKIfurhDI29RqibEicNOoH0WiaCuKb6jWppVu4uzWovO0d1ICAwuW4rB4zfUxVvGHfNuxXLHu44t3yBkgbicQ/132', '$2a$10$ayDiCbzqQkYEh.dlQjWEx.uWeX3mh//OLfFkpVdvGF0fxBElGagcG', '0', '0', '127.0.0.1', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', 'ry', '2020-03-15 17:33:47', '管理员');
INSERT INTO `sys_user` VALUES ('2', 'ry', '若依', '00', 'ry@qq.com', '15666666666', '1', 'https://wx.qlogo.cn/mmopen/vi_32/1yzTKJKIfurhDI29RqibEicNOoH0WiaCuKb6jWppVu4uzWovO0d1ICAwuW4rB4zfUxVvGHfNuxXLHu44t3yBkgbicQ/132', '$2a$10$LP25Pp2GITfV3QX6v.MmT.jV7vi.1rnQQJeeab3/AIyJn2.kPFiOq', '0', '0', '127.0.0.1', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', 'admin', '2020-03-16 22:03:17', '测试员');
INSERT INTO `sys_user` VALUES ('100', 'test', 'test', '00', 'test@qq.com', '13192294560', '0', 'https://wx.qlogo.cn/mmopen/vi_32/1yzTKJKIfurhDI29RqibEicNOoH0WiaCuKb6jWppVu4uzWovO0d1ICAwuW4rB4zfUxVvGHfNuxXLHu44t3yBkgbicQ/132', '$2a$10$fLcdPrdYZn9hNhTCFfMxBe0YhUYzDZgcvR89UGxDA.AnyzK1r6av.', '0', '2', '', null, 'admin', '2020-03-14 15:55:16', 'admin', '2020-03-14 15:58:17', null);
INSERT INTO `sys_user` VALUES ('101', '11', '111', '00', '11@qqw.bb', '13111111111', '0', 'https://wx.qlogo.cn/mmopen/vi_32/1yzTKJKIfurhDI29RqibEicNOoH0WiaCuKb6jWppVu4uzWovO0d1ICAwuW4rB4zfUxVvGHfNuxXLHu44t3yBkgbicQ/132', '$2a$10$EWDLVRkUmBX2If/eCgwjJODuDjAPiX7ZDKEDfTgfwrUh68/h3L/6S', '0', '0', '', null, 'admin', '2020-03-14 16:01:08', '', null, null);
INSERT INTO `sys_user` VALUES ('102', '2222', '222', '00', '22@Qq.mm', '13122222222', '0', 'https://wx.qlogo.cn/mmopen/vi_32/1yzTKJKIfurhDI29RqibEicNOoH0WiaCuKb6jWppVu4uzWovO0d1ICAwuW4rB4zfUxVvGHfNuxXLHu44t3yBkgbicQ/132', '$2a$10$bws6QiPyKuqDGjgfivN6B.37WcvpUmyaj4RnmH3vHduSp7.nJXN7i', '0', '0', '', null, 'admin', '2020-03-14 16:01:50', '', null, null);
INSERT INTO `sys_user` VALUES ('103', 'test11', '2', '00', '22@qq.com', '13222332222', '0', '', '$2a$10$bCUZogMaUn7Gmr3A2OE5Ue/PI3lqqRHJbVnS1b8bPB1smIXyIEOU6', '0', '2', '', null, 'admin', '2020-03-14 16:03:02', '', null, null);
INSERT INTO `sys_user` VALUES ('104', 'test1', 'test', '00', 'test1@qq.com', '13315554215', '0', '', '123456', '0', '0', '', null, 'admin', '2020-03-16 22:16:36', 'admin', '2020-03-16 22:22:44', null);
INSERT INTO `sys_user` VALUES ('105', 'l1_1_1', 'l1_1_1', '00', 'l1_1_1@qq.com', '13192294561', '1', '', '123456', '0', '0', '', null, 'system', '2020-03-21 15:02:37', '', '2020-03-29 23:05:39', null);
INSERT INTO `sys_user` VALUES ('109', 'asdf', 'asdd', '00', 'asd@q.com', '13311122311', null, null, '$2a$10$5F3q3LiCkvtMCdJlmG7BnOGN.XrIrjOsgbkNqih/KMZgzEh95uJsG', '0', '0', null, null, 'system', '2020-04-02 08:02:48', null, null, null);
INSERT INTO `sys_user` VALUES ('110', 'asdf', 'asdd', '00', 'asd@q.com', '13311122311', null, null, '$2a$10$.OHjQu4OkMB4I2o6DMHKDuZ6yk1rNlPwU3oloKVa4YEXGXqxeT7nK', '0', '0', null, null, 'system', '2020-04-02 08:02:48', null, null, null);
INSERT INTO `sys_user` VALUES ('111', 'asdf', 'asdd', '00', 'asd@q.com', '13311122311', null, null, '$2a$10$t66gnYmlYo8jBjdTTel0LeeSBUQ0vG186j6Eqjje8lbsctVz74g3y', '0', '0', null, null, 'system', '2020-04-02 08:02:48', null, null, null);
INSERT INTO `sys_user` VALUES ('112', 'asdf', 'asdd', '00', 'asd@q.com', '13311122311', null, null, '$2a$10$mW9l7i7mlIS96LXfjzfSnuCfrv042Pr7yGhjw1wc/mc0dYbV2TRbe', '0', '0', null, null, 'system', '2020-04-02 08:02:48', null, null, null);
INSERT INTO `sys_user` VALUES ('113', 'asdas', 'asdas', '00', 'asdasd@qq.com', '13122321123', null, null, '$2a$10$/47VtkXAXxw15JrcvzqK8OzDW0b5xGp8eS4oSw.v7SJNjycv15q3S', '0', '0', null, null, 'system', '2020-04-02 08:03:27', null, null, null);
INSERT INTO `sys_user` VALUES ('114', 'asdasda', 'asdasda', '00', 'asdasda@qq.com', '13412311222', null, 'https://wx.qlogo.cn/mmopen/vi_32/1yzTKJKIfurhDI29RqibEicNOoH0WiaCuKb6jWppVu4uzWovO0d1ICAwuW4rB4zfUxVvGHfNuxXLHu44t3yBkgbicQ/132', '$2a$10$wXRCV6ite.IjeRuhS8g5gOFqjkxBmvYCLVPsFYe/kiSL5Ho6VQKPK', '0', '0', null, null, 'system', '2020-04-02 08:06:55', null, null, null);
INSERT INTO `sys_user` VALUES ('115', 'del_flag', 'del_flag', '00', 'del_flag@qq.com', '15312112311', null, 'https://wx.qlogo.cn/mmopen/vi_32/1yzTKJKIfurhDI29RqibEicNOoH0WiaCuKb6jWppVu4uzWovO0d1ICAwuW4rB4zfUxVvGHfNuxXLHu44t3yBkgbicQ/132', '$2a$10$5KlITzUxdpmeSTm4sx1wvuL5e8eaxrJAMDuXFn0QFL01tF3mwhXLm', '0', '0', null, null, 'system', '2020-04-02 08:08:59', null, null, null);

-- ----------------------------
-- Table structure for `sys_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户和角色关联表';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1', '1');
INSERT INTO `sys_user_role` VALUES ('2', '2');
INSERT INTO `sys_user_role` VALUES ('100', '2');
INSERT INTO `sys_user_role` VALUES ('104', '2');
INSERT INTO `sys_user_role` VALUES ('109', '100');
INSERT INTO `sys_user_role` VALUES ('110', '100');
INSERT INTO `sys_user_role` VALUES ('111', '100');
INSERT INTO `sys_user_role` VALUES ('112', '100');
INSERT INTO `sys_user_role` VALUES ('115', '100');
INSERT INTO `sys_user_role` VALUES ('233', '1');
INSERT INTO `sys_user_role` VALUES ('233', '2');
